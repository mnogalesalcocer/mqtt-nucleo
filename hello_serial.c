/**
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <FreeRTOS.h>
#include <queue.h>
#include <task.h>

#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"
#include "pico/multicore.h"

#include "hardware/structs/rosc.h"
#include "hardware/gpio.h"
#include "hardware/adc.h"

#include <string.h>
#include <time.h>

#include "lwip/pbuf.h"
#include "lwip/tcp.h"
#include "lwip/dns.h"

#include "lwip/altcp_tcp.h"
#include "lwip/altcp_tls.h"
#include "lwip/apps/mqtt.h"

#include "lwip/apps/mqtt_priv.h"

//********** mis defines *************
static QueueHandle_t xQueue = NULL;


#define LED_ROJO        16
#define BUTTON_GPIO_17  17

#define DEBUG_printf printf

#define MQTT_SERVER_HOST "telemetria.inti.gob.ar"
#define MQTT_SERVER_PORT 1883

#define PUBLISH_TOPIC  "inti/dma/pruebas/humedad"

#define WILL_TOPIC "inti/dma/pruebas/temperature"        /*uvyt: ultima voluntad y testamentp*/
#define WILL_MSG "sin conexion a broker"             /*uvyt: ultima voluntad y testamentp*/

#define MS_PUBLISH_PERIOD 5000
#define MQTT_TLS 0 // needs to be 1 for AWS IoT

typedef struct MQTT_CLIENT_T_ {
    ip_addr_t remote_addr;
    mqtt_client_t *mqtt_client;
    u8_t receiving;
    u32_t received;
    u32_t counter;
    u32_t reconnect;
} MQTT_CLIENT_T;


void led_task(void);
void publish_task(void* pvParameters);

err_t mqtt_app_connect(MQTT_CLIENT_T *client);
void perform_payload( char *p);

const uint LED_PIN = 16;
const uint DOOR_SWITCH_PIN = 17;
const uint MAX_TIMINGS = 85;


// Perform initialisation
static MQTT_CLIENT_T* mqtt_client_init(void)
{ 
    MQTT_CLIENT_T *client = calloc(1, sizeof(MQTT_CLIENT_T));
    
    if (!client) {
        DEBUG_printf("failed to allocate state\n");
        return NULL;
    }
    client->receiving = 0;
    client->received = 0;
    return client;
}

void dns_found(const char *name, const ip_addr_t *ipaddr, void *callback_arg)
{
    MQTT_CLIENT_T *client = (MQTT_CLIENT_T*)callback_arg;
    DEBUG_printf("DNS query finished with resolved addr of %s.\n", ip4addr_ntoa(ipaddr));
    client->remote_addr = *ipaddr;
}

void run_dns_lookup(MQTT_CLIENT_T *client)
{
    DEBUG_printf("Running DNS query for %s.\n", MQTT_SERVER_HOST);

    cyw43_arch_lwip_begin();
    err_t err = dns_gethostbyname(MQTT_SERVER_HOST, &(client->remote_addr), dns_found, client);
   cyw43_arch_lwip_end();

    if (err == ERR_ARG) {
        DEBUG_printf("failed to start DNS query\n");
        return;
    }

    if (err == ERR_OK) {
        DEBUG_printf("no lookup needed");
        return;
    }

    while (client->remote_addr.addr == 0) {
        cyw43_arch_poll();
        sleep_ms(1);
    }
}

static void mqtt_connection_cb(mqtt_client_t *client, void *arg, mqtt_connection_status_t status)
{
    if (status != 0) {
        DEBUG_printf("Error during connection: err %d.\n", status);
    } else {
        DEBUG_printf("MQTT connected.\n");
    }
}

/* 
 * Called when publish is complete either with success or failure
 */
void mqtt_pub_request_cb(void *arg, err_t err) {
    MQTT_CLIENT_T *client = (MQTT_CLIENT_T *)arg;
    printf("request publish ERROR %d \n", err);
    client->receiving = 0;
    client->received++;
}

/*
 * The app publishing
 */
err_t mqtt_app_publish(MQTT_CLIENT_T *client, char *payload)
{
    err_t err;
    u8_t qos = 2;       /* 0 1 or 2, see MQTT specification */
    u8_t retain = 0;

    // prepare payload to publish
    sprintf(payload, "hello from picow %d/%d", client->received, client->counter);
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);      //prendo led al enviar dato
    cyw43_arch_lwip_begin();
    err = mqtt_publish(client->mqtt_client, PUBLISH_TOPIC , payload, strlen(payload), qos, retain, mqtt_pub_request_cb, client);
    cyw43_arch_lwip_end();
    //cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);       //apago led
    if(err != ERR_OK) {
        DEBUG_printf("**** Publish fail***** %d/%d\n", err, ERR_OK);
        cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0); 
    }
    else {
        printf("**** Publish ok ***** %d/%d\n", err, ERR_OK);
    }

    return err;
}

/* Initiate client and connect to server, if this fails immediately an error code is returned
 * otherwise mqtt_connection_cb will be called with connection result after attempting to
 * to establish a connection with the server. For now MQTT version 3.1.1 is always used
 */
err_t mqtt_app_connect(MQTT_CLIENT_T *client)
{
    struct mqtt_connect_client_info_t ci;
    err_t err;

    memset(&ci, 0, sizeof(ci));

    ci.client_id = "PicoW";
    ci.client_user = NULL;
    ci.client_pass = NULL;
    ci.keep_alive = 0;
    ci.will_topic = WILL_TOPIC;
    ci.will_msg = WILL_MSG;
    ci.will_retain = 0;
    ci.will_qos = 2;

    err = mqtt_client_connect(client->mqtt_client, &(client->remote_addr), MQTT_SERVER_PORT, mqtt_connection_cb, client, &ci);

    if (err != ERR_OK) {
        DEBUG_printf("ERROR en mqtt_app_connect() %d \n",err);
    }

    return err;
}


void perform_payload( char *p)
{
    time_t s;
    struct tm* current_time;

    char status[10];
       
    if( gpio_get(DOOR_SWITCH_PIN) ){
        gpio_put(LED_PIN, 1);
        strcpy(status,"ON");
    }
    else{
        gpio_put(LED_PIN, 0);
        strcpy(status,"OFF");
    }

    // time in seconds
    s = time(NULL);
     // to get current time
    current_time = localtime(&s);
 
    sprintf( p, "%02d:%02d:%02d-%s",
           current_time->tm_hour,
           current_time->tm_min,
           current_time->tm_sec,
           status
           );
}



//*******************************************************************
void led_task()
{
        //cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);  //prendo led
       // vTaskDelay(1000);  // Delay by TICKS defined by FreeRTOS priorities
        //cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
       // vTaskDelay(1000);
    gpio_init(LED_ROJO); 
    gpio_set_dir(LED_ROJO,GPIO_OUT); 
    while(1)
    {
        gpio_put(LED_ROJO, 1);
        vTaskDelay(2000);
        gpio_put(LED_ROJO, 0);
        vTaskDelay(1000);
    }
}

//*******************************************************************
void publish_task(void *pvParameters)
{
    MQTT_CLIENT_T *client = (MQTT_CLIENT_T *)pvParameters;
    // ERR_ISCONN=-10 	Conn already established. 
    int cont= 0;
    err_t error;
    u32_t publish_period = MS_PUBLISH_PERIOD; // in ms

    char payload[128];

    if (client->mqtt_client == NULL)
    {
        DEBUG_printf("Failed to create new mqtt client\n");
    }
    else {
            printf("create new MQTT client OK \n");
    }

    DEBUG_printf("Inicio publish \n");

        //vTaskDelay(10000);
    while(true)
    {
          cyw43_arch_poll();
          vTaskDelay(10000);
          //printf("taskdelay");
           int aux= mqtt_client_is_connected(client->mqtt_client);
           
           printf(" 1 si es connected %d\n", aux);
           if (aux) {
                   // perform_payload( payload );
                    error = mqtt_app_publish(client,"HOLA");   // <-- Publish!
                    printf("published %d \n", client->counter);
                    if ( error == ERR_OK) {
                        DEBUG_printf("publicacion num: %d\n", client->counter);
                        client->counter++;
                    } 
           }
          else{
                //mqtt_disconnect(client->mqtt_client); 	
                // vTaskDelay(10000);
                 cont++;
               //client->mqtt_client = mqtt_client_new();
               //error System.out.println("hello interface 2");

}


int main()
{
   // char WIFI_SSID[] = "wifi01-ei";
   // char WIFI_PASSWORD[] = "Ax32MnF1975-ReB";    //contraseña valida
    stdio_init_all();
    if (cyw43_arch_init())
    {
        printf("Wi-Fi init failed");
        return -1;
    }
    else{
            printf("Wi-Fi init success");
    }
    cyw43_arch_enable_sta_mode();
        
    //*****************************************************************
    DEBUG_printf("Connecting to WiFi...\n");
    if (cyw43_arch_wifi_connect_timeout_ms(WIFI_SSID, WIFI_PASSWORD, CYW43_AUTH_WPA2_AES_PSK, 30000)) {
        DEBUG_printf("failed to  connect.\n");
        return 1;
    } else {
        DEBUG_printf("*** WIFI Connected.*** \n");
    }


    MQTT_CLIENT_T *my_client = mqtt_client_init();
    run_dns_lookup(my_client); 

    my_client->mqtt_client = mqtt_client_new();
    my_client->counter = 0;

    if (mqtt_app_connect(my_client) == ERR_OK)
    {
        DEBUG_printf("exito mqtt_Connect()\n");
    }
    else{
            printf("MQTT no conectado... ERROR= %d /n", ERR_OK);
        }

     /*xTaskCreate(
        publish_task,  // Task to be run
        "PUBLISH_TASK",      // Name of the Task for debugging and managing its Task Handle
        2048,             // Stack depth to be allocated for use with task's stack (see docs)
        my_client,            // Arguments needed by the Task (NULL because we don't have any)
        configMAX_PRIORITIES-1,               // Task Priority - Higher the number the more priority [max is (configMAX_PRIORITIES - 1) provided in FreeRTOSConfig.h]
        NULL              // Task Handle if available for managing the task
    );*/

    xTaskCreate(
        led_task,  // Task to be run
        "LED_task",      // Name of the Task for debugging and managing its Task Handle
        128,             // Stack depth to be allocated for use with task's stack (see docs)
        NULL,            // Arguments needed by the Task (NULL because we don't have any)
        tskIDLE_PRIORITY,               // Task Priority - Higher the number the more priority [max is (configMAX_PRIORITIES - 1) provided in FreeRTOSConfig.h]
        NULL             // Task Handle if available for managing the task
    );

// Create a 3rd task
    // Force it to run on core1
    TaskHandle_t taskpublish_handle;
    UBaseType_t uxCoreAffinityMask;
    xTaskCreate(publish_task, "PUBLISH_TASK", 256,my_client, tskIDLE_PRIORITY-1, &( taskpublish_handle ));
    // To force to run on core0:
    // uxCoreAffinityMask = ( ( 1 << 0 ));
    uxCoreAffinityMask = ( ( 1 << 0 ));
    vTaskCoreAffinitySet( taskpublish_handle, uxCoreAffinityMask );


    DEBUG_printf("*** inicio scheduler*** \n");
    vTaskStartScheduler();
    cyw43_arch_deinit();

    return 0;
}
